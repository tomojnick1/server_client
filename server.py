import socket as s
import json
import datetime


class Server:
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.BUFFER = 1024
        self.info = "version 1.0.0 - date:06.09.2023"
        self.help = "possible choices: \n'info' - returns version of the server and issue date \n'uptime' - returns time of server activity \n'stop' - break connection "
        self.stop = "connection broken"
        self.uptime = datetime.datetime.now()

    def connection(self):
        self.server_socket = s.socket(s.AF_INET, s.SOCK_STREAM)
        self.server_socket.bind((self.host, self.port))
        self.server_socket.listen(2)
        self.client_socket, self.address = self.server_socket.accept()

    def process_request(self, client_request):
        if client_request == "info":
            return self.info
        elif client_request == "help":
            return self.help
        elif client_request == "uptime":
            return str(datetime.datetime.now() - self.uptime)
        elif client_request == "stop":
            self.client_socket.send(json.dumps(self.stop).encode("utf8"))
            return True
        else:
            return "no response"

    def send_response(self, response):
        self.client_socket.send(json.dumps(response).encode("utf8"))

    def close_connection(self):
        self.client_socket.close()
        self.server_socket.close()


if __name__ == "__main__":
    HOST = "192.168.0.115"
    PORT = 33000
    server = Server(HOST, PORT)
    server.connection()
    while True:
        print(
            f"Udało sie nawiązać połączenie z {server.address[0]}:{server.address[1]}"
        )
        client_request = json.loads(
            server.client_socket.recv(server.BUFFER).decode("utf8")
        )
        response = server.process_request(client_request)
        server.send_response(response)
        if response is True:
            server.close_connection()
            break
