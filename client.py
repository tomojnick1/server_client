import socket as s
import json


class Client:
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.BUFFER = 1024

    def connection(self):
        self.client_socket = s.socket(s.AF_INET, s.SOCK_STREAM)
        self.client_socket.connect((self.host, self.port))

    def send_request(self, request):
        self.client_socket.send(json.dumps(request).encode("utf8"))

    def receive_response(self):
        return json.loads((self.client_socket.recv(self.BUFFER).decode("utf8")))

    def close_connection(self):
        self.client_socket.close()


if __name__ == "__main__":
    HOST = "192.168.0.115"
    PORT = 33000
    client = Client(HOST, PORT)
    client.connection()
    while True:
        client_request = input()
        client.send_request(client_request)
        server_answer = client.receive_response()
        print(server_answer)
